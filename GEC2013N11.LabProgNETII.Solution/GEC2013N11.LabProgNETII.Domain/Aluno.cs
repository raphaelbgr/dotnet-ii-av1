﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEC2013N11.LabProgNETII.Domain
{
    public class Aluno
    {
        private int alunoId;

        public int AlunoId
        {
            get { return alunoId; }
            set { alunoId = value; }
        }

        private string nome;

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string sobrenome;

        public string Sobrenome
        {
            get { return sobrenome; }
            set { sobrenome = value; }
        }

        private string nomeDoCurso;

        public string NomeDoCurso
        {
            get { return nomeDoCurso; }
            set { nomeDoCurso = value; }
        }

        private bool ativo;

        public bool Ativo
        {
            get { return ativo; }
            set { ativo = value; }
        }


    }
}
