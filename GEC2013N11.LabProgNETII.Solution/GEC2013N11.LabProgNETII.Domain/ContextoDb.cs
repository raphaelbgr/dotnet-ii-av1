﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEC2013N11.LabProgNETII.Domain
{
    public class ContextoDb : DbContext
    {
        public DbSet<Aluno> Alunos { get; set; }
    }
}
