﻿using GEC2013N11.LabProgNETII.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GEC2013N11.LabProgNETII.Presentation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        ContextoDb db = new ContextoDb();

        public MainWindow()
        {
            InitializeComponent();
            dataGrid.ItemsSource = db.Alunos.ToList();
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            Aluno aluno = new Aluno();
            aluno.Nome = textBox.Text;
            aluno.Sobrenome = textBox1.Text;
            aluno.NomeDoCurso = textBox2.Text;
            aluno.Ativo = radioButton.IsChecked.Value;

            if (Validade(aluno))
            {
                Task<object> task1 = Task.Run<object>(() =>
                {
                    db.Alunos.Add(aluno);
                    db.SaveChanges();
                    return new object();
                });
                object o = await task1;
                UpdateGrid();
            }
            else
            {
                MessageBox.Show("Todos os campos são obrigatórios.");
            }
        }

        private bool Validade(Aluno aluno)
        {
            if (aluno.Nome != null && aluno.Nome != ""
                && aluno.Sobrenome != null && aluno.Sobrenome != ""
                && aluno.NomeDoCurso != null && aluno.NomeDoCurso != "")
            {
                return true;
            }
            return false;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            UpdateGrid();
        }

        private void AutoSave(object sender, DataGridCellEditEndingEventArgs e)
        {
            db.SaveChanges();
        }

        private async void UpdateGrid()
        {
            db.SaveChanges();
            Task<List<Aluno>> task = Task.Run<List<Aluno>>(() => db.Alunos.ToList());
            dataGrid.ItemsSource = await task;
        }

        private void Update(object sender, DataTransferEventArgs e)
        {
            UpdateGrid();
        }

        private void AutoSave(object sender, SelectedCellsChangedEventArgs e)
        {
            db.SaveChanges();
        }

        private void AutoSave(object sender, SelectionChangedEventArgs e)
        {
            db.SaveChanges();
        }
    }
}
